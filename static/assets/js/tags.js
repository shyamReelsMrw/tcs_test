function initTagSelection() {
    const tagInput = document.getElementById('id_tags');
    const tagList = document.getElementById('tag-list');

    tagInput.addEventListener('input', (event) => {
        const searchTerm = event.target.value.toLowerCase();
        let tags = [];

        for (const tagOption of tagList.options) {
            if (tagOption.text.toLowerCase().includes(searchTerm)) {
                tags.push(tagOption);
            }
        }

        tagList.innerHTML = '';
        for (const tag of tags) {
            tagList.appendChild(tag.cloneNode(true));
        }
    });
}

document.addEventListener('DOMContentLoaded', initTagSelection);

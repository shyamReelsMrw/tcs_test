from django.db import models
from django.contrib.auth.models import AbstractUser, Group, Permission
from django.utils.translation import gettext_lazy as _

# Create your models here.


class Account(AbstractUser):

    email = models.EmailField(_('Email'), unique=True, max_length=20)
    username = models.CharField(_('Username'), unique=True, max_length=20)
    groups = models.ManyToManyField(Group, blank=True, related_name='account_groups')
    user_permissions = models.ManyToManyField(Permission, blank=True, related_name='account_user_permissions')

    REQUIRED_FIELDS = ['username']
    USERNAME_FIELD = 'email'

    class Meta:
        verbose_name = 'Account'
        verbose_name_plural = 'Account'

    def __str__(self):
        return self.first_name + self.last_name

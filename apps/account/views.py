from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.views.generic import View, TemplateView, RedirectView
from django.contrib.auth import get_user_model, login, logout, authenticate
from django.contrib.messages.views import SuccessMessageMixin
from django.contrib import messages


# Create your views here.


class LoginView(View):

    def get(self, request):
        return render(request, 'login.html')

    def post(self, request):
        if request.method == 'POST':
            email = request.POST.get('email')
            password = request.POST.get('password')
            try:
                user = get_user_model()
                user = user.objects.get(email=email)
            except Exception as e:
                messages.error(request, e)
            user = authenticate(email=email, password=password)
            if user is not None:
                login(request, user)
                messages.success(request, "Login Successfull")
                return HttpResponseRedirect('/')
            else:
                messages.error(request, "Password Incorrect")
        return render(request, 'login.html')


class LogoutView(RedirectView):

    url = '/'
    
    def get(self, request, *args, **kwargs):
        logout(request)
        return super().get(request, *args, **kwargs)
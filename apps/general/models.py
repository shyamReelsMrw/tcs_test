from django.db import models
from django_extensions.db.models import TimeStampedModel
from ckeditor.fields import RichTextField
from autoslug import AutoSlugField
from django.contrib.auth.models import User
from django.contrib.auth import get_user_model
from ckeditor.fields import RichTextField
# Create your models here.


User = get_user_model()


class ArticleCategory(TimeStampedModel):
    name = models.CharField(max_length=30, blank=True, null=True)
    slug = AutoSlugField(populate_from='name', unique=True)
    is_active = models.BooleanField(default=True)

    class Meta:
        verbose_name = 'Article Category'
        verbose_name_plural = 'Article Category'

    def __str__(self):
        return "%s" % self.name

    def get_absolute_url(self):
        return "news/%s/" % self.slug


class ArticleTag(TimeStampedModel):
    name = models.CharField(max_length=20)
    slug = AutoSlugField(populate_from='name', unique=True)
    is_active = models.BooleanField(default=True)
    
    objects = models.Manager()

    class Meta:
        verbose_name = 'Article Tag'
        verbose_name_plural = 'Article Tag'

    def __str__(self) -> str:
        return "%s" % self.name

    def get_absolute_url(self):
        return "/tag/%s/" % self.slug


class Article(TimeStampedModel):

    DRAFT = 'draft'
    PUBLISHED = 'published'

    STATUS_CHOICES = (
        (DRAFT, 'Draft'),
        (PUBLISHED, 'Published')
    )

    title = models.CharField(max_length=500, verbose_name='Headline', db_index=True)
    slug = AutoSlugField(populate_from='title', unique=True)
    category = models.ForeignKey(ArticleCategory, null=True, blank=True, related_name='articles',
                                 on_delete=models.SET_NULL, db_index=True)
    tags = models.ManyToManyField(ArticleTag, blank=True, null=True)
    cover_image = models.ImageField(blank=True, null=True)
    short_description = models.TextField(null=True, blank=True)
    description = RichTextField(null=True, blank=True)
    creator = models.ForeignKey(User, on_delete=models.SET_NULL, null=True)
    status = models.CharField(max_length=20, choices=STATUS_CHOICES, default=PUBLISHED)
    publish_date = models.DateTimeField(null=True, blank=True, auto_created=True, db_index=True)

    # objects = models.Manager()

    class Meta:
        verbose_name = 'Article'
        verbose_name_plural = 'Article'
        permissions = (
            ('can_add_article', "CAN ADD ARTICLE"),
            ('can_edit_article', "CAN EDIT ARTICLE"),
            ('can_delete_article', "CAN DELETE ARTICLE")
        )

    def __str__(self):
        return "%s" % self.title

    def get_article_title(self):
        if self.category:
            return "/news/%s/%s" % (self.category.name, self.title)
        else:
            return "/news/%s/" % self.slug

    def get_cover_image(self):
        return self.cover_image

    def get_short_head_line(self):
        return self.title[:50]

    def get_short_description(self):
        return self.short_description[:200]

    def get_absolute_url(self):
        return "/article/%s/" % self.slug


class Contact(models.Model):

    name = models.CharField(max_length=200)
    email = models.EmailField(max_length=200)
    subject = models.CharField(max_length=200)   
    message = models.TextField()

    class Meta:
        verbose_name = "Contact Data"
        verbose_name_plural = "Contact Data"

    def __str__(self):
        return '%s' % self.email

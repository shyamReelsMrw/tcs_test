from collections.abc import Iterable, Mapping
from typing import Any
from django import forms
from django.core.files.base import File
from django.db.models.base import Model
from django.forms.utils import ErrorList
from django.forms.widgets import Textarea, Widget
from .models import Contact, Article, ArticleTag, ArticleCategory, Contact
from django_select2 import forms as s2forms
from django_select2.forms import ModelSelect2MultipleWidget, ModelSelect2TagWidget, Select2Widget, ModelSelect2Widget, Select2MultipleWidget
from django.db.models import Q
from functools import reduce
from .widgets import MultipleSelect2Widget, BootstrapRadioWidget, Select2WidgetField, CKEditorWidget


class CustomModelMultipleChoiceField(ModelSelect2Widget):
    def filter_queryset(self, request, term, queryset=None, **dependent_fields):
        """
        this method is overriden for changing the default behaviour of
        splitting the term. We search for the raw term only.
        """
        if queryset is None:
            queryset = self.get_queryset()
        search_fields = self.get_search_fields()
        select = Q()
        term = term.replace('\t', ' ')
        term = term.replace('\n', ' ')

        select &= reduce(lambda x, y: x | Q(**{y: term}), search_fields[1:],
                         Q(**{search_fields[0]: term}))
        if dependent_fields:
            select &= Q(**dependent_fields)

        return queryset.filter(select).distinct()


class ArticleTagSelect2Widget(ModelSelect2TagWidget):

    queryset = ArticleTag.objects.all()
    search_fields = ('name__icontains',)

    # def value_from_datadict(self, data, files, name):
    #     values = set(super().value_from_datadict(data, files, name))
    #     print(values)
    #     names = self.queryset.filter(**{'name__in': list(values)}).values_list('name', flat=True)
    #     cleaned_values = list(names)
    #     for val in values - set(list(names)):
    #         cleaned_values.append(self.queryset.create(name=val).name)
    #     return cleaned_values

    def filter_queryset(self, request, term, queryset=None, **dependent_fields):
        """
        this method is overriden for changing the default behaviour of
        splitting the term. We search for the raw term only.
        """
        if queryset is None:
            queryset = self.get_queryset()
        search_fields = self.get_search_fields()
        select = Q()
        term = term.replace('\t', ' ')
        term = term.replace('\n', ' ')

        select &= reduce(lambda x, y: x | Q(**{y: term}), search_fields[1:],
                         Q(**{search_fields[0]: term}))
        if dependent_fields:
            select &= Q(**dependent_fields)

        return queryset.filter(select).distinct()


class ArticleCategoryWidget(CustomModelMultipleChoiceField):

    search_fields = [
        "name__icontains"
    ]


class ArticleTagWidget(s2forms.ModelSelect2MultipleWidget):

    search_fields = [
        "name__icontains"
    ]


class ArticleForm(forms.ModelForm):

    category = forms.ModelChoiceField(queryset=ArticleCategory.objects.all(),
                                      widget=Select2Widget(attrs={"class": "form-control"}))
    tags = forms.ModelMultipleChoiceField(queryset=ArticleTag.objects.all(),
               widget=ArticleTagSelect2Widget(attrs={"class":"form-control"}))
    # tags = forms.ModelMultipleChoiceField(queryset=ArticleTag.objects.all(),
    # widget=Select2MultipleWidget(attrs={"class":"form-control"}))

    class Meta:
        model = Article
        fields = ['title', 'category', 'tags', 'cover_image', 'short_description', 'description']

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        for name, field in self.fields.items():
            field.widget.attrs.update({'class': 'form-control'})


class EmailSendForm(forms.Form):

    name = forms.CharField(max_length=100, widget=forms.TextInput(attrs={
        'placeholder': 'Your Name', 'width': '100%', 'class': 'form-control'
    }))

    email = forms.EmailField(max_length=100, required=True, widget=forms.EmailInput(attrs={
        'placeholder': 'Your Email', 'width': '100%', 'class': 'form-control'
    }))

    subject = forms.CharField(max_length=100, widget=forms.TextInput(attrs={
        'placeholder': 'Subject', 'width': '100%', 'class': 'form-control'
    }))

    message = forms.CharField(widget=forms.Textarea(attrs={
        'placeholder': 'Message', 'class': 'form-control', 'width': '100%', 'rows': '5'
    }))

    def save(self):
        name = self.cleaned_data['name']
        email = self.cleaned_data['email']
        subject = self.cleaned_data['subject']
        message = self.cleaned_data['message']
        email_message = Contact.objects.create(**self.cleaned_data)
        email_message.save()


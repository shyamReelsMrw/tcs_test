from django.forms import widgets
from django.utils.safestring import mark_safe


class MultipleSelect2Widget(widgets.Select):

    # template_name = 'widgets/select2_widget.html'

    def __init__(self, attrs=None, choices=()):
        if attrs is None:
            attrs = {}
        attrs['style'] = 'width: 200px'
        super().__init__(attrs, choices)

    def render(self, name, value, attrs=None, renderer=None):
        renderer = super().render(name, value, attrs, renderer)
        script = f"""
                    <script type="text/javascript">
                        $(document).ready(function() {{
                            $('#id_{name}').select2();
                            $('#id_{name}').on('select2:opening select2:closing', function(event) {{
                                var $searchfield = $(this).parent().find('.select2-search__field');
                                $searchfield.prop('disabled', true);
                            }});
                        }});
                    </script>
                """
        return renderer + script


class Select2WidgetField(widgets.Widget):
#
    template_name = 'widgets/select2_widget.html'
    pass
#
#     def __init__(self, attrs=None, choices=()):
#         self.choices = choices
#         print(attrs)
#         if attrs is None:
#             attrs = {}
#         super().__init__(attrs)
#
#     def get_context(self, name, value, attrs):
#         context = super().get_context(name, value, attrs)
#         context['widget']['choices'] = self.choices
#         return context


class CKEditorWidget(widgets.Textarea):

    template_name = 'widgets/ck_widget.html'

    def __init__(self, attrs=None):
        default_attrs = {
            "rows": "5"
        }
        if attrs:
            default_attrs.update(attrs)
        super().__init__(default_attrs)


class BootstrapRadioWidget(widgets.Widget):

    template_name = 'widgets/radio_widget.html'

    def __init__(self, options, attrs=None):
        self.options = options
        super().__init__(attrs)

    def get_context(self, name, value, attrs):
        context = super().get_context(name, value, attrs)
        context['widget']['options'] = self.options
        return context


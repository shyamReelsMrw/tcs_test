from typing import Any, Optional, Type
from django.forms.forms import BaseForm
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import redirect, render
from django.http import Http404
from django.core.mail import send_mail
from django.conf import settings
from django.contrib.auth.decorators import login_required
from django.urls import reverse
from .forms import EmailSendForm, ArticleForm
from django.views.generic import TemplateView, FormView, CreateView, View
from django.core.paginator import Paginator
from .models import Article, ArticleCategory, ArticleTag
from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import get_object_or_404
# Create your views here.


class Home(TemplateView):

    form_class = EmailSendForm
    template_name = "index.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['form'] = self.form_class
        return context


class ArticleView(TemplateView):

    template_name = 'blog.html'
    paginator = Paginator
    paginated_by = 4

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        page_number = self.request.GET.get('page', 1)
        paginator = self.paginator(Article.objects.all(), self.paginated_by)
        context['articles'] = paginator.get_page(page_number)
        return context


class ArticleDetailView(TemplateView):

    template_name = 'blog-single.html'
    
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        slug = kwargs.get('slug')
        context['article'] = Article.objects.get(slug=slug)
        return context


class ArticleCategoryCreateView(LoginRequiredMixin, CreateView):

    def post(self, request, *args, **kwargs):
        name = request.POST.get('name')
        article_category = ArticleCategory.objects.create(name=name)
        return HttpResponse('/')


class ArticleTagCreateView(LoginRequiredMixin, CreateView):

    def post(self, request, *args, **kwargs):
        name = request.POST.get('name')
        article_tag = ArticleTag.objects.create(name=name)
        return HttpResponse('/')


class CategoryList(TemplateView):
    template_name = 'test.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        # You should replace 'your_category_name' with the actual category name you want to filter by.
        category_name = kwargs.get('cat')
        try:
            category = ArticleCategory.objects.get(name=category_name)  # Use get() to retrieve a single instance
            articles = category.articles.all()
            context['articles'] = articles
        except ArticleCategory.DoesNotExist:
            raise Http404("Category does not exist")  # Handle the case where the category doesn't exist
        return context


class TagList(TemplateView):

    template_name = ''

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        tag_slug = kwargs.get('tag')
        tag = ArticleTag.objects.get(slug=tag_slug)
        context['article'] = Article.objects.filter(tags=tag)
        return context


class PortfolioDetails(TemplateView):

    template_name = "portfolio-details.html"


class PrivacyPolicy(TemplateView):

    template_name = "privacy-policy.html"


class ContactForm(FormView):

    form_class = EmailSendForm

    def post(self, request, *args, **kwargs):
        if request.method == 'POST':
            print('test')
            form = self.form_class(request.POST)
            if form.is_valid():
                form.save()
            return render(request, 'index.html', context={'error': 'error'})
        return render(request, 'index.html', context={'success': 'success'})


class ArticleCreateView(LoginRequiredMixin, CreateView):
    
    template_name = 'blog_form.html'
    form_class = ArticleForm

    def get_context_data(self, **kwargs: Any) -> dict[str, Any]:
        context = super().get_context_data(**kwargs)
        context['form'] = self.form_class
        return context
    
    def form_valid(self, form_class):
        print(self.request.POST.get('tags'))
        article = form_class.save(commit=False)
        article.creator = self.request.user
        article.save()
        form_class.save_m2m()
        # tags = self.request.POST.get("tags").replace(',', ' ').split()
        # for t in tags:
        #     t, created = ArticleTag.objects.get_or_create(name=t)
        #     article.tags.add(t)
        # article.save()
        super().form_valid(form_class)
        return redirect('blog')


class EditArticleView(TemplateView):

    template_name = 'blog_form.html'
    edit_form_class = ArticleForm

    def post(self, request, *args, **kwargs):
        print("test", self.request.POST.get('title'))
        context = self.get_context_data(**kwargs)
        obj = get_object_or_404(Article, slug=kwargs.get('slug'))
        form = self.edit_form_class(request.POST, request.FILES, instance=obj)
        article = form.save(commit=False)
        article.save()
        form.save_m2m()
        context['form'] = form
        self.render_to_response(context)
        return redirect('home')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        obj = get_object_or_404(Article, slug=kwargs.get('slug'))
        form = self.edit_form_class(instance=obj)
        context['form'] = form
        return context


class CategoryCreate(LoginRequiredMixin, View):

    def post(self, request, *args, **kwargs):
        name = request.POST.get('category_name')

        if name is not None:
            print("False")
            article_cat = ArticleCategory.objects.create(name=name)
            article_cat.save()
            return redirect('add-article')
        else:
            # Handle the case where name is None, perhaps with an error message or redirect
            return HttpResponse("Category name is required.")


class TagCreateView(LoginRequiredMixin, View):

    def post(self, request, *args, **kwargs):
        name = request.POST.get('tag_name')
        article_tag = ArticleTag.objects.create(name=name)
        article_tag.save()
        return redirect('add-article')



from django.contrib import admin
from .models import Contact, ArticleTag, ArticleCategory, Article
from .forms import CustomModelMultipleChoiceField
from django import forms


class ArticleCategoryAdmin(admin.ModelAdmin):

    search_fields = ['name']


class ArticleTagAdmin(admin.ModelAdmin):

    search_fields = ['name']


class ArticleAdmin(admin.ModelAdmin):

    autocomplete_fields = ['category', 'tags']


# Register your models here.
admin.site.register(ArticleCategory, ArticleCategoryAdmin)
admin.site.register(ArticleTag, ArticleTagAdmin)
admin.site.register(Article, ArticleAdmin)
admin.site.register(Contact)

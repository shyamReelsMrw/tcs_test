from django.db.models.signals import Signal, post_save
from django.dispatch import receiver
from django.core.mail import send_mail, send_mass_mail
from .models import Contact
from django.conf import settings
from django.core import mail
from django.template.loader import render_to_string, get_template
from datetime import datetime
from django.utils.html import strip_tags

send_email = Signal()


@receiver(post_save, sender=Contact)
def send_email(sender, instance, created, **kwargs):
    if created:

        html_message = render_to_string('thankyou.html')
        response_message = render_to_string('formdetails.html', context={'from': instance.email, 'subject': instance.subject,
                                                                         'message': instance.message})
        message = mail.EmailMultiAlternatives(
            instance.subject,
            html_message,
            settings.EMAIL_HOST_USER,
            [instance.email]
        )
        message_1 = mail.EmailMultiAlternatives(
            instance.subject,
            response_message,
            settings.EMAIL_HOST_USER,
            ['totalcaretvm@gmail.com']
        )

        message_1.content_subtype = "html"
        message.content_subtype = "html"

        try:
            connection = mail.get_connection()
            connection.open()
            connection.send_messages([message, message_1])
            connection.close()
        except Exception as e:
            print(e)
            
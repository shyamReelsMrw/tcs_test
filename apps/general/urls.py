from django.urls import path
from .views import Home, ArticleView, ArticleDetailView, PortfolioDetails, PrivacyPolicy, ContactForm, \
                CategoryList, ArticleCreateView, CategoryCreate, TagCreateView, EditArticleView

urlpatterns = [
    path('', Home.as_view(), name="home"),
    path("articles/", ArticleView.as_view(), name="blog"),
    path("article/<slug:slug>/", ArticleDetailView.as_view(), name="blog_single"),
    path("portfolio_details/", PortfolioDetails.as_view(), name="portfolio_details"),
    path("privacy_policy/", PrivacyPolicy.as_view(), name="privacy_policy"),
    path("contact_form/", ContactForm.as_view(), name="contact_form"),
    path("news/<str:cat>/", CategoryList.as_view()),
    path("edit-article/<slug:slug>/", EditArticleView.as_view()),
    path("add-article/", ArticleCreateView.as_view(), name='add-article'),
    path("add-category/", CategoryCreate.as_view(), name="add-category"),
    path("add-tag/", TagCreateView.as_view(), name="add-tag")
]

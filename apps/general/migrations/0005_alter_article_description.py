# Generated by Django 4.1.5 on 2023-11-07 18:03

import ckeditor.fields
from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('general', '0004_alter_article_description'),
    ]

    operations = [
        migrations.AlterField(
            model_name='article',
            name='description',
            field=ckeditor.fields.RichTextField(blank=True, null=True),
        ),
    ]

# Generated by Django 4.1.5 on 2023-10-16 13:59

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('general', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='article',
            options={'permissions': (('can_add_article', 'CAN ADD ARTICLE'), ('can_edit_article', 'CAN EDIT ARTICLE'), ('can_delete_article', 'CAN DELETE ARTICLE')), 'verbose_name': 'Article', 'verbose_name_plural': 'Article'},
        ),
    ]
